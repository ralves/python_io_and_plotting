class: center, middle

.presentationtitle[
# Python II <br>IO and plotting
]

Renato Alves - EMBL Heidelberg

BTM - October 2016

[https://git.embl.de/ralves/python_io_and_plotting](https://git.embl.de/ralves/python_io_and_plotting)

---

# Keep on it to avoid losing it

At EMBL we currently have:

*Python User Group* aka PUG  
Roughly bi-weekly, Wednesdays, ~1 hour  
toby.hodges@embl.de - https://bio-it.embl.de/

*Coding Club* (not Python specific)  
Weekly, Thursdays, ~2 hours (or whatever you want to dedicate)  
jonas.hartmann@embl.de - https://bio-it.embl.de/coding-club/

*Bio-IT drop-in sessions & training*  
Weekly, Thursdays, 10:00-12:00, staff lounge  
toby.hodges@embl.de - https://bio-it.embl.de/

---

# Setting up - from Python I

If you installed anaconda, create a new project folder, open a terminal in that folder and run `jupyter notebook`.

???

Show of hands for who did the installation

--

If you didn't or have problems running it use [http://ralves.embl.de:8888](http://ralves.embl.de:8888).
Create a folder with your name and then a new Python 3 document.

???

Shared setup - behave or we'll all suffer :)

--

During the presentation we'll have several moments to try things out.
Use Jupyter to make things easier.

???

Brief intro on how to use Jupyter. Write in the cell and press Ctrl/Shift + Enter.

---

# What is IO?

IO stands for Input/Output. The term is used whenever you want to refer to the action or reading or writing data from your program.

```python
# A file with one number per line

myfile = "/data/myresult.txt"

cutoff = 5.0

data = {
    "bigger": [],
    "smaller": [],
}

*with open(myfile) as f:
*   for line in f:
*       number = float(line)
*       if number > cutoff:
*           data["bigger"].append(number)
*       else:
*           data["smaller"].append(number)

bigmean = sum(data["bigger"]) / len(data["bigger"])
smallmean = sum(data["smaller"]) / len(data["smaller"])

*print("The mean of my big data is:", bigmean)
*print("The mean of my small data is:", smallmean)
```

---

# Reading data

The most frequent case will be reading data from files.
You do so by using the `open` function and then read text or binary.

--

```python
# Text file
with open("myfile.txt") as fh:
    for line in fh:
        ...
```

--

```python
# Binary file (e.g. an image)
with open("myfile.png", 'b') as fh:
    # Reads entire file to memory
    data = fh.read()
    ...
```

--

If the files have a complex structure you will find that reading them like this will give you something that is not so easy to work with.

---

# Reading fasta

Take for instance a Fasta file, commonly used to represent biological sequences.

```fasta
>geneA
ACTCTGCACGTGCAGTGTATCGATGCTAGCTAGAACCTGTAGCTAGCTGTAGCTGATCGCT
AGCTAGCTAGCTAATATAGTCGATGCTA

>geneB
ACTGATGCGGTGTGCAGTAGCTAGAACCTGTAGCTAGCTGTAGCTGATCGCT
```

--

Looking at the file you see headers starting with `>` and sequences spanning one or more lines.

To read this text format, you need some `if` statements.

???

If the `line` you are reading is a header
If part of the current sequence
If beginning of a new sequence.

--

You also need a data structure to store the data in memory. Perhaps `{"header": "sequence", ...}`.

--

This kind of code is generally called a *parser*.

--

Luckily, there are parsers available for most common formats and even some less common ones.
These can either be part of the *standard library* or as third-party modules developed by the [community of Python programmers](https://pypi.python.org/pypi).

???

*standard library* - what ships with every python installation
General advice - Google "python formatname" before anything

---

# Reading Fasta

Using BioPython:

```python
from Bio import SeqIO

myfasta = "../shared/sequences.fasta"

for record in SeqIO.parse(myfasta, format="fasta"):
    print(record.id, record.seq)
    ...
```

---

# Common formats

Formats vary widely between fields but here's a list of common ones:

```nohighlight
CSV and TSV       - import csv
XML               - from xml.etree import ElementTree
JSON              - import json
HTML              - from html.parser import HTMLParser
INI               - import configparser
Excel           @ - import pyexcel
Fasta & Fastq   @ - from Bio import SeqIO  # Biopython
PDB             @ - from Bio import SeqIO  # Biopython
Seq. Alignment  @ - from Bio import SeqIO  # Biopython
SAM/BAM         @ - import pysam
Images          @ - from PIL import Image  # Pillow
HDF5            @ - import h5py
BioFormats      @ - import bioformats
...
```

**Note**: `@` modules are not part of the *standard library*.

This list is not exhaustive. Much more at [PyPi](https://pypi.python.org/pypi).

---

# Numeric data

When dealing with numeric data, CSV (comma separated values) and TSV (tab separated values) are commonly used formats.
These generally represent a single spreadsheet with one or more columns.

--

A generic solution would use the [`csv`](https://docs.python.org/3/library/csv.html) module to read these.

```python
import csv

with open("../shared/medals.csv") as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",", quotechar="'")
    for row in csvreader:
        print(row)

# Would produce
['Place', 'Country', 'Gold', 'Silver', 'Bronze', 'Total']
['1', 'United States (USA)', '46', '37', '38', '121']
['2', 'Great Britain (GBR)', '27', '23', '17', '67']
...
```

--

*Note* the use of `delimiter` and `quotechar`. That's because `csv` is a [flexible format](https://en.wikipedia.org/wiki/Comma-separated_values#Basic_rules) with quite a few variants.

---

# Numeric data

With most data analysis packages: `numpy`, `pandas`, ... There's often specific functions that read them and store the data in `numpy.array` or `pandas.DataFrame`.

```python
numpy.loadtxt(...)
numpy.fromfile(...)
pandas.read_csv(...)
pandas.read_table(...)
```

--

Make sure to check documentation of these functions either online, by using `help(function)` or simply `function?` in Jupyter/Ipython.

```python
help(numpy.loadtxt)
    Load data from a text file.

    Each row in the text file must have the same number of values.

    Parameters
    ----------
    fname : file or str
        File, filename, or generator to read.  If the filename extension is
        ``.gz`` or ``.bz2``, the file is first decompressed. Note that
        generators should return byte strings for Python 3k.
    ...
```

---

# Plotting

Plotting is *the art* of visually representing your data.

???

I mean art because it's not just about looking pretty, that helps to capture attention, but the important part is to get the message across.

--

The same data can be represented in different ways.

.inline[![:scale 50%](static/img/scatter.png)![:scale 50%](static/img/cloud.png)]

.credits[from [Seaborn](https://seaborn.github.io/tutorial.html) tutorials]

--

You could even overlay multiple plots...

---

# Plotting libraries

Contrary to other languages such as R and MatLab, plotting is not a built-in functionality of Python.

--

This means Plotting in python is attained via third-party modules.

The most popular is by far [matplotlib](http://matplotlib.sourceforge.net/).

--

Other options include [Bokeh](http://bokeh.pydata.org/) and [Plotly](https://plot.ly/python/) which focus on web presentation as well as [Mayavi](http://code.enthought.com/projects/mayavi/) and [VTK](http://www.vtk.org/) for sophisticated 3D representations.

???

VTK and Mayavi are commonly used for physics simulations.

--

Additionally there are some that build on top of these, [Seaborn](https://seaborn.github.io/) being one example.

---

# Seaborn

`seaborn` is built around `matplotlib`. It mostly provides additional types of plots and makes the result look nicer.

--

`matplotlib` default style is a little bit on the ugly side. Seaborn provides something nice to start with.

.inline[![:scale 50%](static/img/matplot_hist.png)![:scale 50%](static/img/seaborn_hist.png)]

--

However, seaborn has limited functionality and if you need to customize it beyond what is provided you are back with `matplotlib`.

---

# Matplotlib

Matplotlib very powerful due to being completely customizable.  
You can do anything with it.

--

Even somewhat complicated things.

.center[![:scale 55%](static/img/sankey_demo_rankine.png)]

---

# Matplotlib

On the other hand, it can be a little difficult due to the many things that can be changed.  
The documentation is great but to cover everything it can't be small.

--

Some, not so obvious things:

.center[![:scale 50%](static/img/fig_map.png)]

.credits[from [matplotlib usage](http://matplotlib.org/faq/usage_faq.html) documentation]

---

# Matplotlib

But not all is difficult:

```python
%matplotlib inline

from matplotlib import pyplot as plt

a = [1,2,3,4,5]

plt.figure()
plt.subplot(1, 1, 1)     # num_rows, num_cols, plot_number
plt.title("Dots")
plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.scatter(x=a, y=a)
plt.savefig("dots.png")
```

---

# Seaborn

And you can also use `seaborn`:

```python
# Create some data
import numpy as np
np.random.seed(1)

a = np.random.normal(size=100)

# Do the actual plotting
import seaborn as sns

sns.distplot(a, kde=False, rug=True)

iris = sns.load_dataset("iris")
sns.pairplot(iris);
```

---

# Reference

* [Matplotlib gallery](http://matplotlib.org/gallery.html)
* [Matplotlib tutorial](http://matplotlib.org/users/pyplot_tutorial.html)
* [Seaborn gallery](https://seaborn.github.io/examples/index.html)
* [Seaborn tutorial](https://seaborn.github.io/tutorial.html)
